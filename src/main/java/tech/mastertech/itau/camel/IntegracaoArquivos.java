package tech.mastertech.itau.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class IntegracaoArquivos {
	public static void main(String[] args) throws Exception {
		CamelContext contexto = new DefaultCamelContext();
		contexto.addRoutes(new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				from("file://origem/?recursive=true&noop=true")
				.delay(15_000)
				.to("file://destino")
				.end();
			}
		});
		
		contexto.start();
		
		for(;;) {}
	}
}
