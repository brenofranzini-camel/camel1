package tech.mastertech.itau.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class IntegracaoArquivos2 {
	public static void main(String[] args) throws Exception {
		CamelContext contexto = new DefaultCamelContext();
		contexto.addRoutes(new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				errorHandler(defaultErrorHandler().maximumRedeliveries(2)
//						.redeliveryDelay(2_000)
						.onExceptionOccurred(new Processor() {

							@Override
							public void process(Exchange exchange) throws Exception {
								System.out.println("Deu ruim na cópia! \n");
								System.out.println(
										"Tentativa:" + exchange.getMessage().getHeader(Exchange.REDELIVERY_COUNTER));
								System.out.println("Erro: " + exchange.getException());
							}
						}));

				from("file://origem/?recursive=true&antInclude=*.txt")
//				.delay(7_000)
						.process(new Processor() {

							@Override
							public void process(Exchange exchange) throws Exception {
								String nomeArquivo = exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);
								exchange.getIn().setHeader(Exchange.FILE_NAME, nomeArquivo + ".bk");

								nomeArquivo = exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);
								System.out.printf("\nTentando copiar o arquivo %s \n", nomeArquivo);
							}
						}).to("file://destino")
						.end();
			}
		});
		contexto.start();

		for (;;) {
		}
	}
}